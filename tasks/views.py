from asyncio import tasks
from django.views.generic import CreateView, ListView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from tasks.models import Task
from django.urls import reverse


# Create your views here.
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create_task.html"
    fields = ["name", "start_date", "project", "due_date", "assignee"]

    def get_success_url(self):
        return reverse("show_project", args=[self.object.project.id])


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/update_task.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list_tasks.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
